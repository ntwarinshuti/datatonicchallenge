package com.datatonic.cloud.dataflow.clementnn_challenge;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.options.Validation;
import com.google.cloud.dataflow.sdk.transforms.Count;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.transforms.PTransform;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.KV;
import com.google.cloud.dataflow.sdk.values.PCollection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by clementnn on 8/1/16.
 * Adds the following rows to the flighs_dataset.datatonic BigQuery Table
 *   - day_of_week
 *   - day_of_month
 *
 * <p>To execute this pipeline using the Dataflow service, use the following command:
 * <pre>{@code
 * mvn compile exec:java \
 *  -Dexec.mainClass=com.datatonic.cloud.dataflow.clementnn_challenge.ExtendFlightsDataset \
 *  -Dexec.args="--project=big-structure-138023 \
 *  --stagingLocation=gs://big-structure-138023-dataflow/staging/ \
 *  --runner=BlockingDataflowPipelineRunner"
 * }
 * </pre>
 * and the BigQuery table for the output:
 * <pre>{@code
 *   --output=YOUR_PROJECT_ID:DATASET_ID.TABLE_ID
 * }</pre>
 *
 * <p>The BigQuery input table defaults to {@code big-structure-138023:flighs_dataset.datatonic_sample}
 * and can be overridden with {@code --input}.
 */

public class ExtendFlightsDataset {
    // Default to using a 1000 row subset of the flighs_dataset.datatonic table.
    private static final String FLIGHTS_DATASET_SAMPLE =
            "big-structure-138023:flighs_dataset.datatonic_sample";
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    // Schema of the BigQuery table containing the flight dataset.
    private static final String[][] TABLE_SCHEMA =
            new String[][]{
                    {"date", "STRING"},
                    {"airline", "STRING"},
                    {"airline_code", "STRING"},
                    {"departure_airport", "STRING"},
                    {"departure_state", "STRING"},
                    {"departure_lat", "FLOAT"},
                    {"departure_lon", "FLOAT"},
                    {"arrival_ariport", "STRING"},
                    {"arrival_state", "STRING"},
                    {"arrival_lat", "FLOAT"},
                    {"arrival_lon", "FLOAT"},
                    {"departure_schedule", "INTEGER"},
                    {"departure_actual", "INTEGER"},
                    {"departure_delay", "FLOAT"},
                    {"arrival_schedule", "INTEGER"},
                    {"arrival_actual", "INTEGER"},
                    {"arrival_delay", "FLOAT"}};

    /**
     * Process a row of the BigQuery table and extends if with two columns:
     *  - day_of_week
     *  - day_of_month
     */
    static class ExtractDayInformationFn extends DoFn<TableRow, TableRow> {
        private static final Logger LOG = LoggerFactory.getLogger(ExtendFlightsDataset.class);
        @Override
        public void processElement(ProcessContext c) throws ParseException {
            TableRow row = c.element();
            TableRow extendedRow = new TableRow();
            // Initialize extendedRow
            String columnType;
            for (String[] column: TABLE_SCHEMA) {
                // Add column value to row after appropriate casting.
                columnType = column[1];
                switch (columnType) {
                    case "INTEGER":
                        // For some unknown reason, integers are stored as strings and need to be parsed.
                        // TODO(clementnn) Add counter for number of null values.
                        String numberToParse = (String) row.get(column[0]);
                        if (numberToParse == null) { break; }
                        extendedRow.set(column[0], Integer.parseInt((String) row.get(column[0])));
                        break;
                    case "FLOAT":
                        extendedRow.set(column[0], row.get(column[0]));
                        break;
                    default:
                        extendedRow.set(column[0], row.get(column[0]));
                        break;
                }
            }
            String dateString = (String) row.get("date");
            String dayOfWeek = getDayOfWeek(dateString);
            int dayOfMonth = getDayOfMonth(dateString);
            extendedRow.set("day_of_week", dayOfWeek);
            extendedRow.set("day_of_month", dayOfMonth);
            c.output(extendedRow);
        }
    }

    static String getDayOfWeek(String dateString) throws ParseException {
        Date date = new SimpleDateFormat(DATE_FORMAT).parse(dateString);

        // Format Date to day of week
        SimpleDateFormat dowFormatter = new SimpleDateFormat("EE");
        String dayOfWeek = dowFormatter.format(date);
        return dayOfWeek;
    }

    static int getDayOfMonth(String dateString) throws ParseException {
        Date date = new SimpleDateFormat(DATE_FORMAT).parse(dateString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Takes rows from a table and extends it with day of week and day of month
     * information.
     */
    static class ExtendTable
            extends PTransform<PCollection<TableRow>, PCollection<TableRow>> {
        @Override
        public PCollection<TableRow> apply(PCollection<TableRow> rows) {
            PCollection<TableRow> extendedRows = rows.apply(ParDo.of(new ExtractDayInformationFn()));

            return extendedRows;
        }
    }

    public static interface Options extends PipelineOptions {
        @Description("Table to extend, specified as "
            + "<project_id>:<dataset_id>,<table_id>")
        @Default.String(FLIGHTS_DATASET_SAMPLE)
        String getInput();
        void setInput(String value);

        @Description("BigQuery table to write to, specified as"
            + "<project_id>:<dataset_id>,<table_id>.")
        @Default.String(FLIGHTS_DATASET_SAMPLE + "_extended")
        String getOutput();
        void setOutput(String value);
    }

    public static void main(String[] args) {
        Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);

        Pipeline p = Pipeline.create(options);

        // Build the table schema for the output table.
        List<TableFieldSchema> fields = new ArrayList<>();
        // Add the columns of the original table.
        for (String[] column : TABLE_SCHEMA) {
            fields.add(new TableFieldSchema().setName(column[0]).setType(column[1]));
        }
        // Add extensions columns
        fields.add(new TableFieldSchema().setName("day_of_week").setType("STRING"));
        fields.add(new TableFieldSchema().setName("day_of_month").setType("INTEGER"));
        TableSchema schema = new TableSchema().setFields(fields);

        p.apply(BigQueryIO.Read.from(options.getInput()))
                .apply(new ExtendTable())
                .apply(BigQueryIO.Write.to(options.getOutput())
                    .withSchema(schema)
                    .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                    .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE));

        p.run();
    }
}
