package com.datatonic.cloud.dataflow.clementnn_challenge;

import com.google.api.services.bigquery.model.Table;
import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.transforms.DoFnTester;
import com.google.cloud.dataflow.sdk.values.KV;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Test case for {@link ExtendFlightsDataset}
 */
public class ExtendFlightsDatasetTest {

    @Test
    public void testGetDayOfWeek() throws Exception {
        String testDate = "2016-08-02";
        String expected = "Tue";
        String computed = ExtendFlightsDataset.getDayOfWeek(testDate);
        Assert.assertEquals(expected, computed);
    }

    @Test
    public void testGetDayOfMonth() throws Exception {
        String testDate = "2016-08-02";
        int expected = 2;
        int computed = ExtendFlightsDataset.getDayOfMonth(testDate);
        Assert.assertEquals(expected, computed);
    }
}
