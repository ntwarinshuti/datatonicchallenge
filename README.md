# Roadmap 
1. Perform analysis on the dataset using datalab
	1. Flights arrival by
		1. State
		1. Time of day [bucketized]
		1. Day of week
		1. Day of month
		1. Month
	1. Flight Departure by ...
	1. Delay time (bucketized or averaged)

1. Extend dataset using data present [here](http://www.transtats.bts.gov/DL_SelectFields.asp?Table_ID=236&DB_Short_Name=On-Time)
	1. Add Cancelled and Cancelled Code
	1. Add cause of delay

1. Add new features
	1. Day of week
	1. Day of month
	1. Departure delay bucketized

1. Build predictive models for
	1. Is plane going to be delayed (definition of delay as greater than X minutes)
	1. Reason for delay

1. Try different models
	1. Logistic regression
	1. Neural networks (dead end, because no tensorflow for the moment)
	1. Linear regression
	1. Decision trees
	1. Other?

1. If really have time: Build a recommendation system
	1. Givent departure time and airport, arrival time and airport recommend which airline to use.

# Task logs

## 20160721 - 21:00-23:00
* Created a Google Cloud Platform account
* Created a project 'DatatonicChallenge'
* Tried upload dataset into
	* BigTable -> to expensive (1.95/hour for 3 nodes)
	* BigQuery -> could not upload a large csv file. Need to go through cloud storage
	* Cloud Storage
		* Led to installation of cloud SDK because file was too large to upload on the web UI
		* Stopping work now because the file is uploading
* FOR NEXT TIME
	* Test to see if upload of composite objects work
	* Create BigQuery using the data in cloud storage
	* Analyze data on the BigQuery table using datalab (see analysis steps in Roadmap)
	* Download extra data and add it to cloud storage/BigQuery

* FOR LATER
	* Dataflow code to extend the BigQuery table (just to showcase skills, this could be done in SQL)
	* Build predictive models
		* Linear: logistic
		* Decision trees
		* Naive bayes

## 20160726 - 20:00-21:30
* Started to perform preliminary analaysis on the dataset using datalab
	* Crash course on how pandas, BigQuery and SQL can be used in datalab
	* Looked at distributions of average, mininimum and maximum delays by airline company
* Requested access to Cloud Machine Learning Platform

* FOR NEXT TIME
	* Continue analysis. Perform same analysis slicing by STATE, DAY_OF_WEEK, DAY_OF_MONTH, DEPARTURE_HOUR
	* Discard previous idea of downloading and adding extra data.

* FOR LATER
	* Dataflow
	* Machine learning in tensorflow if I got access to ML platform.

## 20160727 - 19:00-20:30
* Took a break from analysis and started working on the dataflow code.
	* Reinstalled google cloud sdk and maven.
	* Browsed through docs to get a feel of dataflow.
	* Run WordCount to test that installation went well.
	* Looked at docs on how to read & write data to BigQuery from dataflow

* FOR NEXT TIME
	* Implement dataflow code to create a BigQuery similar to flighs_dataset.datatonic with extra rows:
		* DAY OF WEEK (use formula from [here](https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week))
		* DAY OF MONTH
		* Local hour of day (hour of day taking timezone into account)
		* Hour of day without timezone

* FOR LATER
	* Continue analysis
	* Discard idea of ML with tensorflow. Might have been too optimistic on the time this would take.

## 20160801 - 20:00-22:00
* Finished implement of dataflow to extend flighs_dataset table with two extra rows.
	* Realized that arrival and departure were already in local time so don't need to that anymore.
	* Dropped the idea of converting to GMT time. To much of a pain.

* FOR NEXT TIME
	* Finish analysis.

## 20160801 - 19:30-22:00
* Continued analysis. Reduced scope of analyses performed to the following
	1. Number of flights by
		1. Departure state
		1. Arrival state
		1. Day of week
		1. Day of month
		1. Departure state X Arrival state
	1. Average arrival delay by
		1. Departure state
		1. Arrival state
		1. Day of week
		1. Day of month
		1. Departure state X Arrival state
